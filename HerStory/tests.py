from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from django.http import HttpRequest
import time

# Create your tests here.

class HerStoryUnitTest(TestCase):
    def test_my_story_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_my_story_using_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
